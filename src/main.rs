use std::collections::HashMap;
use std::num::ParseIntError;
use std::rc::Rc;

#[derive(Debug, PartialEq, Default)]
struct Node {
    id: u32,
    left: Option<Rc<Node>>,
    right: Option<Rc<Node>>,
}

impl Node {
    pub fn depth(&self) -> usize {
        self.left
            .as_ref()
            .map(|node| node.depth() + 1)
            .unwrap_or_default()
            + self
                .right
                .as_ref()
                .map(|node| node.depth() + 1)
                .unwrap_or_default()
    }
}

#[derive(Debug, Default, PartialEq)]
struct Statistics {
    depth_per_node: HashMap<u32, usize>,
    average_depth: usize,
    average_node_ref: usize,
}

#[derive(Debug, PartialEq)]
struct DirectAcyclicGraph {
    nodes: HashMap<u32, Rc<Node>>,
    stats: Statistics,
}

impl DirectAcyclicGraph {
    pub fn from_str(contents: &str) -> Result<Self, String> {
        let mut lines = contents.lines().enumerate();
        let size = lines
            .next()
            .map(|(_, line)| line)
            .and_then(|s| s.parse::<usize>().ok())
            .unwrap_or_default();

        let mut ref_per_node = HashMap::new();
        let mut nodes = HashMap::new();
        let mut stats = Statistics::default();
        while let Some((idx, line)) = lines.next() {
            if idx > size {
                break;
            }
            let parsed: Vec<Result<u32, ParseIntError>> = line
                .trim()
                .split(' ')
                .map(|s| s.trim().parse::<u32>())
                .collect();
            let id = parsed
                .get(0)
                .cloned()
                .ok_or_else(|| "Missing node id".to_string())?
                .map_err(|e| format!("Id does not have valid valid. {}", e))?;
            let left_id = parsed
                .get(1)
                .cloned()
                .ok_or_else(|| "Missing left node id".to_string())?
                .map_err(|e| format!("Left node does not have valid valid. {}", e))?;
            if left_id > id {
                return Err("Potential cycle detected on left child".to_string());
            }
            if nodes.get(&left_id).is_some() {
                *ref_per_node.entry(left_id).or_insert(0) += 1;
            }
            let right_id = parsed
                .get(2)
                .cloned()
                .ok_or_else(|| "Missing right node id".to_string())?
                .map_err(|e| format!("Right node does not have valid valid. {}", e))?;
            if right_id > id {
                return Err("Potential cycle detected on right child".to_string());
            }
            if nodes.get(&right_id).is_some() {
                *ref_per_node.entry(right_id).or_insert(0) += 1;
            }
            nodes.insert(
                id,
                Rc::new(Node {
                    id,
                    left: nodes.get(&left_id).cloned(),
                    right: nodes.get(&right_id).cloned(),
                }),
            );
        }
        for (id, node) in nodes.iter() {
            stats.depth_per_node.insert(*id, node.depth());
        }
        stats.average_depth = {
            let max = stats.depth_per_node.iter().map(|e| *e.1).sum::<usize>();
            max / std::cmp::max(stats.depth_per_node.len(), 1)
        };
        stats.average_node_ref = {
            let max = ref_per_node.iter().map(|e| *e.1).sum::<usize>();
            max / std::cmp::max(stats.depth_per_node.len(), 1)
        };
        Ok(DirectAcyclicGraph { nodes, stats })
    }
}

fn main() -> Result<(), String> {
    let filename = std::env::args()
        .next()
        .ok_or_else(|| "Source file name is required".to_string())?;
    let contents = std::fs::read_to_string(filename).map_err(|e| format!("{}", e))?;
    let dag = DirectAcyclicGraph::from_str(contents.as_str())?;
    println!("{:?}", dag);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn empty_dag() {
        let contents = r#"0"#;
        let dag = DirectAcyclicGraph::from_str(contents);

        let nodes = HashMap::new();
        let stats = Statistics::default();
        let expected = DirectAcyclicGraph { nodes, stats };
        assert_eq!(dag, Ok(expected));
    }

    #[test]
    fn single_node() {
        let contents = r#"1
        1 0 0"#;
        let dag = DirectAcyclicGraph::from_str(contents);
        let mut nodes = HashMap::new();
        nodes.insert(
            1,
            Rc::new(Node {
                id: 1,
                left: None,
                right: None,
            }),
        );
        let mut depth_per_node = HashMap::new();
        depth_per_node.insert(1, 0);
        let stats = Statistics {
            depth_per_node,
            average_node_ref: 0,
            average_depth: 0,
        };
        let expected = DirectAcyclicGraph { nodes, stats };
        assert_eq!(dag, Ok(expected));
    }

    #[test]
    fn single_left_parent_node() {
        let contents = r#"2
        1 0 0
        2 1 0"#;
        let dag = DirectAcyclicGraph::from_str(contents);
        let mut nodes = HashMap::new();
        nodes.insert(
            1,
            Rc::new(Node {
                id: 1,
                left: None,
                right: None,
            }),
        );
        nodes.insert(
            2,
            Rc::new(Node {
                id: 2,
                left: nodes.get(&1).cloned(),
                right: None,
            }),
        );
        let mut depth_per_node = HashMap::new();
        depth_per_node.insert(2, 1);
        depth_per_node.insert(1, 0);
        let stats = Statistics {
            depth_per_node,
            average_node_ref: 0,
            average_depth: 0,
        };
        let expected = DirectAcyclicGraph { nodes, stats };
        assert_eq!(dag, Ok(expected));
    }

    #[test]
    fn two_parent_node() {
        let contents = r#"3
        1 0 0
        2 1 0
        3 2 1"#;
        let dag = DirectAcyclicGraph::from_str(contents);
        let mut nodes = HashMap::new();
        nodes.insert(
            1,
            Rc::new(Node {
                id: 1,
                left: None,
                right: None,
            }),
        );
        nodes.insert(
            2,
            Rc::new(Node {
                id: 2,
                left: nodes.get(&1).cloned(),
                right: None,
            }),
        );
        nodes.insert(
            3,
            Rc::new(Node {
                id: 3,
                left: nodes.get(&2).cloned(),
                right: nodes.get(&1).cloned(),
            }),
        );
        let mut depth_per_node = HashMap::new();
        depth_per_node.insert(3, 3);
        depth_per_node.insert(2, 1);
        depth_per_node.insert(1, 0);
        let stats = Statistics {
            depth_per_node,
            average_node_ref: 1,
            average_depth: 1,
        };
        let expected = DirectAcyclicGraph { nodes, stats };
        assert_eq!(dag, Ok(expected));
    }

    #[test]
    fn second_layer_node() {
        let contents = r#"4
        1 0 0
        2 1 0
        3 2 1
        4 2 2"#;
        let dag = DirectAcyclicGraph::from_str(contents);
        let mut nodes = HashMap::new();
        nodes.insert(
            1,
            Rc::new(Node {
                id: 1,
                left: None,
                right: None,
            }),
        );
        nodes.insert(
            2,
            Rc::new(Node {
                id: 2,
                left: nodes.get(&1).cloned(),
                right: None,
            }),
        );
        nodes.insert(
            3,
            Rc::new(Node {
                id: 3,
                left: nodes.get(&2).cloned(),
                right: nodes.get(&1).cloned(),
            }),
        );
        nodes.insert(
            4,
            Rc::new(Node {
                id: 4,
                left: nodes.get(&2).cloned(),
                right: nodes.get(&2).cloned(),
            }),
        );
        let mut depth_per_node = HashMap::new();
        depth_per_node.insert(4, 4);
        depth_per_node.insert(3, 3);
        depth_per_node.insert(2, 1);
        depth_per_node.insert(1, 0);
        let stats = Statistics {
            depth_per_node,
            average_node_ref: 1,
            average_depth: 2,
        };
        let expected = DirectAcyclicGraph { nodes, stats };
        assert_eq!(dag, Ok(expected));
    }

    #[test]
    fn full_example() {
        let contents = r#"7
            1 0 0
            2 1 0
            3 2 1
            4 2 2
            5 3 4
            6 1 2
            7 5 6"#;
        let dag = DirectAcyclicGraph::from_str(contents);
        let mut nodes = HashMap::new();
        nodes.insert(
            1,
            Rc::new(Node {
                id: 1,
                left: None,
                right: None,
            }),
        );
        nodes.insert(
            2,
            Rc::new(Node {
                id: 2,
                left: nodes.get(&1).cloned(),
                right: None,
            }),
        );
        nodes.insert(
            3,
            Rc::new(Node {
                id: 3,
                left: nodes.get(&2).cloned(),
                right: nodes.get(&1).cloned(),
            }),
        );
        nodes.insert(
            4,
            Rc::new(Node {
                id: 4,
                left: nodes.get(&2).cloned(),
                right: nodes.get(&2).cloned(),
            }),
        );
        nodes.insert(
            5,
            Rc::new(Node {
                id: 5,
                left: nodes.get(&3).cloned(),
                right: nodes.get(&4).cloned(),
            }),
        );
        nodes.insert(
            6,
            Rc::new(Node {
                id: 6,
                left: nodes.get(&1).cloned(),
                right: nodes.get(&2).cloned(),
            }),
        );
        nodes.insert(
            7,
            Rc::new(Node {
                id: 7,
                left: nodes.get(&5).cloned(),
                right: nodes.get(&6).cloned(),
            }),
        );
        let mut depth_per_node = HashMap::new();
        depth_per_node.insert(7, 14);
        depth_per_node.insert(6, 3);
        depth_per_node.insert(5, 9);
        depth_per_node.insert(4, 4);
        depth_per_node.insert(3, 3);
        depth_per_node.insert(2, 1);
        depth_per_node.insert(1, 0);
        let stats = Statistics {
            depth_per_node,
            average_node_ref: 1,
            average_depth: 4,
        };
        let expected = DirectAcyclicGraph { nodes, stats };
        assert_eq!(dag, Ok(expected));
    }
}
